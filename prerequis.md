<header>
S. de Chaisemartin
</header>

# Prérequis cours CHPS/POOCS

Au cours de cet enseignement vous réaliserez deux types d'actions
- pendant les cours, de nombreux exercices d'application directe seront proposés. 
- pendant les séances de TD, vous travaillerez sur un petit projet de code C++.

Au niveau du matériel, il est donc **nécessaire d'avoir un PC individuel**, de préférence sous Linux, pour chaque séance que ce soit un cours ou un TD. 

## Concernant les outils **informatiques nécessaires** :

### Pour les **exercices**
- pour la plupart des exercices, nous utiliserons un compilateur en ligne, au choix par exemple parmi :
  - https://wandbox.org/, 
  - http://coliru.stacked-crooked.com/, 
  - https://cppinsights.io/,
  - https://techiedelight.com/compiler/,
- certains exercices nécessiteront de compiler à la main dans un terminal. Il faut sur votre machine un compilateur C/C++ récent : 
  - gcc >= 7.3
  - clang >= 6
  - Visual Studio 2017 ou 2019 (préférer Linux...Windows pas possible pour les TDs)
  
### Pour les **TDs**
- Nous travaillerons sur ***Linux exclusivement***. Le mieux serait une **ubuntu** (20 ou 22) pour pouvoir facilement installer les dépendances requises (via ```apt-get install```), qui vous seront indiquées dans les énoncés.   
- Par ailleurs il nous faut un environnement de développement intégré (IDE). Je vous propose de travailler avec CLion de Jetbrains (https://www.jetbrains.com/fr-fr/clion/) qui est utilisable gratuitement pour les étudiants (https://www.jetbrains.com/fr-fr/clion/buy/#discounts?billing=yearly). Installer ce logiciel ne nécessite pas de droits administrateurs sur votre machine.